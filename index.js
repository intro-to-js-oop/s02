//alert("test");


//create student one
let studentOneName = "John";
let studentOneEmail = 'john@mail.com'
let studentOneGrades = [89, 84, 78, 88];

//create student two
let studentTwoName = "Joe";
let studentTwoEmail = 'joe@mail.com'
let studentTwoGrades = [78, 82, 79, 85];

//create student three
let studentThreeName = "Jane";
let studentThreeEmail = 'jane@mail.com'
let studentThreeeGrades = [87, 89, 91, 93];

//create student four
let studentFourName = "Jessie";
let studentFourEmail = 'jessie@mail.com'
let studentFourGrades = [91, 89, 92, 93];

//actions that students may perform
function login(email){
	console.log(`${email} has logged in`)
}

login(studentOneEmail);


function logout(email){
	console.log(`${email} has logged out`)
}

function listGrades(grades) {
	grades.forEach(grade => {
		console.log(grade);
	})
}

//spaghetti code - code that is poorly organized that it becomes impossible to work with

//use an object literal: {}
//ENCAPSULATION - the orgranization of information (properties) and behavior(as methods) to belong to the object that encapsulates them (the scope of encapsulation is deoted by the object literals)

let studentOne = {
	name: 'John',
	email: 'john@mail.com',
	grades: [89, 84, 78, 88],

	//add the functionalities available to a student as a object methods
	//the keyword "this" refers to the object encapsulating the method where "this" is called

	log() {
		console.log(`${this.email} has logged in`)
	},
	logout() {
		console.log(`${this.email} has logged out`)
	},
	listGrades() {
		console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`)
	},

	/*
		Mini - Exercise:
		//Create a function that will get/compute the quarterly average of the studentOne's grades:
	*/

	computeAve(){
		let sum = 0;
		this.grades.forEach(x => {
			sum += x;
		});
		return sum/4
	},

	//Mini - Exercise 2:
		// Create a function that will return true if the average grade is >= 85, false if not
	willPass() {
		if(this.computeAve >= 85){
			return true;
		}
		return false;

		// return this.computeAve >= 85 ? true : false
	},

	//Mini-Exercise 3
	//Create a function call willPassWithHonors() that returns true if the student has passed and their average grade is >= 90. The function return false if either one is not met.

	willPassWithHonors() {
		//return (this.willPass() && this.computeAve >= 90) ? true : false;
		if(this.computeAve() < 85){
			return undefined;
		}
		if(this.computeAve() >= 90){
			return true;
		}
		else
			return false;
	}


}

console.log(`student one's name is ${studentOne.name}`);
console.log(`student one's email is ${studentOne.email}`);
console.log(`student one's grade is ${studentOne.grades}`);



//Activities

/*
1. spaghetti code 
2. var object = {}
3. ENCAPSULATION
4. studentOne.enroll()
5. true
6. key: value
7. true
8. true
9. true
10. true
*/


let studentTwo = {
	name: 'Joe',
	email: 'joe@mail.com',
	grades: [78, 82, 79, 85],
	log() {
		console.log(`${this.email} has logged in`)
	},
	logout() {
		console.log(`${this.email} has logged out`)
	},
	listGrades() {
		console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`)
	},
	computeAve(){
		let sum = 0;
		this.grades.forEach(x => {
			sum += x;
		});
		return sum/4
	},
	willPass() {
		if(this.computeAve >= 85){
			return true;
		}
		return false;	
	},
	willPassWithHonors() {
		if(this.computeAve() < 85){
			return undefined;
		}
		if(this.computeAve() >= 90){
			return true;
		}
		else
			return false;
	}
}

let studentThree = {
	name: 'Jane',
	email: 'jane@mail.com',
	grades: [87, 89, 91, 93],
	log() {
		console.log(`${this.email} has logged in`)
	},
	logout() {
		console.log(`${this.email} has logged out`)
	},
	listGrades() {
		console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`)
	},
	computeAve(){
		let sum = 0;
		this.grades.forEach(x => {
			sum += x;
		});
		return sum/4
	},
	willPass() {
		if(this.computeAve >= 85){
			return true;
		}
		return false;	
	},
	willPassWithHonors() {
		if(this.computeAve() < 85){
			return undefined;
		}
		if(this.computeAve() >= 90){
			return true;
		}
		else
			return false;
	}
}

let studentFour = {
	name: 'Jessie',
	email: 'jessie@mail.com',
	grades: [91, 89, 92, 93],
	log() {
		console.log(`${this.email} has logged in`)
	},
	logout() {
		console.log(`${this.email} has logged out`)
	},
	listGrades() {
		console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`)
	},
	computeAve(){
		let sum = 0;
		this.grades.forEach(x => {
			sum += x;
		});
		return sum/4
	},
	willPass() {
		if(this.computeAve >= 85){
			return true;
		}
		return false;	
	},
	willPassWithHonors() {
		if(this.computeAve() < 85){
			return undefined;
		}
		if(this.computeAve() >= 90){
			return true;
		}
		else
			return false;
	}
}


let classOf1A = 
{
	students :
	[
		{
			name: 'John',
			email: 'john@mail.com',
			grades: [89, 84, 78, 88],
			log() {
				console.log(`${this.email} has logged in`)
			},
			logout() {
				console.log(`${this.email} has logged out`)
			},
			listGrades() {
				console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`)
			},
			computeAve(){
				let sum = 0;
				this.grades.forEach(x => {
					sum += x;
				});
				return sum/4
			},
			willPass() {
				if(this.computeAve >= 85){
					return true;
				}
				return false;	
			},
			willPassWithHonors() {
				if(this.computeAve() < 85){
					return undefined;
				}
				if(this.computeAve() >= 90){
					return true;
				}
				else
					return false;
			}
		},
		{
			name: 'Joe',
			email: 'joe@mail.com',
			grades: [78, 82, 79, 85],
			log() {
				console.log(`${this.email} has logged in`)
			},
			logout() {
				console.log(`${this.email} has logged out`)
			},
			listGrades() {
				console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`)
			},
			computeAve(){
				let sum = 0;
				this.grades.forEach(x => {
					sum += x;
				});
				return sum/4
			},
			willPass() {
				if(this.computeAve >= 85){
					return true;
				}
				return false;	
			},
			willPassWithHonors() {
				if(this.computeAve() < 85){
					return undefined;
				}
				if(this.computeAve() >= 90){
					return true;
				}
				else
					return false;
			}
		},
		{
			name: 'Jane',
			email: 'jane@mail.com',
			grades: [87, 89, 91, 93],
			log() {
				console.log(`${this.email} has logged in`)
			},
			logout() {
				console.log(`${this.email} has logged out`)
			},
			listGrades() {
				console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`)
			},
			computeAve(){
				let sum = 0;
				this.grades.forEach(x => {
					sum += x;
				});
				return sum/4
			},
			willPass() {
				if(this.computeAve >= 85){
					return true;
				}
				return false;	
			},
			willPassWithHonors() {
				if(this.computeAve() < 85){
					return undefined;
				}
				if(this.computeAve() >= 90){
					return true;
				}
				else
					return false;
			}
		},
		{
			name: 'Jessie',
			email: 'jessie@mail.com',
			grades: [91, 89, 92, 93],
			log() {
				console.log(`${this.email} has logged in`)
			},
			logout() {
				console.log(`${this.email} has logged out`)
			},
			listGrades() {
				console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`)
			},
			computeAve(){
				let sum = 0;
				this.grades.forEach(x => {
					sum += x;
				});
				return sum/4
			},
			willPass() {
				if(this.computeAve >= 85){
					return true;
				}
				return false;	
			},
			willPassWithHonors() {
				if(this.computeAve() < 85){
					return undefined;
				}
				if(this.computeAve() >= 90){
					return true;
				}
				else
					return false;
			}
		}
	],

	countHonorStudents(){
		let count = 0;
		for(let i = 0;i < this.students.length; i ++){
			if(this.students[i].willPassWithHonors() == true){
				count += 1;
			}
		}
		return count;
	},

	honorsPercentage(){
		return 100 / this.countHonorStudents();
	},

	retrieveHonorStudentInfo(){
		let honors = []
		for(let i = 0;i < this.students.length; i ++){
			if(this.students[i].willPassWithHonors() == true){
				const obj = {aveGrade:this.students[i].computeAve(), email:this.students[i].email};
				honors.push(obj);
			}
		}
		return honors;
	},

	sortHonorStudentsByGradeDesc(){
		return this.retrieveHonorStudentInfo().sort().reverse();
	}

}